package com.example.podcast.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.podcast.Model.Album;
import com.example.podcast.Model.Artist;
import com.example.podcast.R;

import java.util.List;

public class AlbumAdapter extends BaseAdapter {
    private List<Album> albumList;
    private Context context;

    public AlbumAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return albumList.size();
    }

    @Override
    public Album getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView title, year;
        public ImageView image;
        public Artist artist;

        public MyViewHolder(View view){
            super(view);
            title = (TextView) view.findViewById(R.id.album_title);
            year = (TextView) view.findViewById(R.id.album_year);
            image = (ImageView) view.findViewById(R.id.album_image);
        }
    }

    public AlbumAdapter (List<Album> albumList){
        this.albumList = albumList;
    }


}
