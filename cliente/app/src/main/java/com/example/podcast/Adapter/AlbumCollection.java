package com.example.podcast.Adapter;

import com.example.podcast.Model.Album;
import com.example.podcast.Model.Artist;
import com.example.podcast.R;

import java.util.ArrayList;

public class AlbumCollection {

    public static ArrayList<Album> getAlbum(){
        ArrayList<Album> albums = new ArrayList<>();

        Album a = new Album();
        a.setTitle("Titulo álbum uno");
        a.setDescription("");
        a.setImage(R.drawable.perfil);

        albums.add(a);

        a = new Album();
        a.setTitle("Titulo álbum dos");
        a.setDescription("");
        a.setImage(R.drawable.perfil);
        albums.add(a);

        a = new Album();
        a.setTitle("Titulo álbum tres");
        a.setDescription("");
        a.setImage(R.drawable.perfil);
        albums.add(a);

        a = new Album();
        a.setTitle("Titulo álbum cuatro");
        a.setDescription("");
        a.setImage(R.drawable.perfil);
        albums.add(a);

        a = new Album();
        a.setTitle("Titulo álbum cinco");
        a.setDescription("");
        a.setImage(R.drawable.perfil);
        albums.add(a);

        a = new Album();
        a.setTitle("Titulo álbum seis");
        a.setDescription("");
        a.setImage(R.drawable.perfil);
        albums.add(a);

        return albums;
    }


}
