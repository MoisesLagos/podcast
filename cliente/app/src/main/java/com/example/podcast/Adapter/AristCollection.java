package com.example.podcast.Adapter;

import com.example.podcast.Model.Artist;
import com.example.podcast.R;

import java.util.ArrayList;

public class AristCollection {

    public static ArrayList<Artist> getArtist(){
        ArrayList<Artist> artists = new ArrayList<>();

        Artist a = new Artist();
        a.setName("Moises");
        a.setDescription("Loremp ipsum");
        a.setImage(R.drawable.artistimage);
        artists.add(a);

        a = new Artist();
        a.setName("Moises");
        a.setDescription("Loremp ipsum");
        a.setImage(R.drawable.artistimage);



        a = new Artist();
        a.setName("Juan");
        a.setDescription("Loremp ipsum");
        a.setImage(R.drawable.artistimage);
        artists.add(a);

        a = new Artist();
        a.setName("Abraham");
        a.setDescription("Loremp ipsum");
        a.setImage(R.drawable.artistimage);
        artists.add(a);

        a = new Artist();
        a.setName("Jacob");
        a.setDescription("Loremp ipsum");
        a.setImage(R.drawable.artistimage);
        artists.add(a);

        a = new Artist();
        a.setName("Pedro");
        a.setDescription("Loremp ipsum");
        a.setImage(R.drawable.artistimage);
        artists.add(a);

        a = new Artist();
        a.setName("Isaac");
        a.setDescription("Loremp ipsum");
        a.setImage(R.drawable.artistimage);
        artists.add(a);

        return artists;
    }
}
