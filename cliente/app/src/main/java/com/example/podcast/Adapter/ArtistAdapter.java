package com.example.podcast.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.podcast.Model.Artist;
import com.example.podcast.R;

import java.util.List;

public class ArtistAdapter extends RecyclerView.Adapter<ArtistAdapter.MyViewHolder> {

    private List<Artist> artistList;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView name, description, phone, map;
        public ImageView image;

        public MyViewHolder(View view){
            super(view);
            name = (TextView) view.findViewById(R.id.artist_name);
            image = (ImageView) view.findViewById(R.id.artist_image);
        }

        public void onClick(View view){}
    }

    public ArtistAdapter (List<Artist> artistList){
        this.artistList = artistList;
    }

    @Override
    public ArtistAdapter.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.artista_fila, viewGroup, false);

        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ArtistAdapter.MyViewHolder myViewHolder, int i) {
        Artist artist = artistList.get(i);
        myViewHolder.name.setText(artist.getName());
        myViewHolder.image.setImageResource(artist.getImage());
    }

    @Override
    public int getItemCount() {
        return artistList.size();
    }
}
