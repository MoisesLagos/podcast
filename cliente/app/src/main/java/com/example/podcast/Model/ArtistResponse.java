package com.example.podcast.Model;

import java.util.ArrayList;

public class ArtistResponse {
    private int total_items;
    private Artist artist;
    private ArrayList<Artist> artists;

    public ArtistResponse(int total_items, Artist artist, ArrayList<Artist> artists) {
        this.total_items = total_items;
        this.artist = artist;
        this.artists = artists;
    }

    public int getTotal_items() {
        return total_items;
    }

    public void setTotal_items(int total_items) {
        this.total_items = total_items;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public ArrayList<Artist> getArtists() {
        return artists;
    }

    public void setArtists(ArrayList<Artist> artists) {
        this.artists = artists;
    }

    @Override
    public String toString() {
        return "ArtistResponse{" +
                "total_items=" + total_items +
                ", artist=" + artist +
                ", artists=" + artists +
                '}';
    }
}
