package com.example.podcast.Model;

public class Song {

    private String number, name, duration, file;
    private Album album;

    public Song(String number, String name, String duration, String file, Album album) {
        this.number = number;
        this.name = name;
        this.duration = duration;
        this.file = file;
        this.album = album;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }
}
