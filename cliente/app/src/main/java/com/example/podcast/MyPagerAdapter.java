package com.example.podcast;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.podcast.Fragment.FavoritosFragment;
import com.example.podcast.Fragment.MisPodcastFragment;
import com.example.podcast.Fragment.PodcastFragment;

public class MyPagerAdapter extends FragmentStatePagerAdapter {

    int mNoOfTabs;

    public MyPagerAdapter(FragmentManager fm, int NumberOfTabs){
        super(fm);
        this.mNoOfTabs = NumberOfTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch(position)
        {
            case 0:
                PodcastFragment podcastFragment = new PodcastFragment();
                return podcastFragment;
            case 1:
                FavoritosFragment favoritosFragment= new FavoritosFragment();
                return  favoritosFragment;
            case 2:
                MisPodcastFragment misPodcastFragment= new MisPodcastFragment();
                return  misPodcastFragment;
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Podcast";
            case 1:
                return "Favoritos";
            case 2:
                return "Mis Podcast";
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNoOfTabs;
    }
}
