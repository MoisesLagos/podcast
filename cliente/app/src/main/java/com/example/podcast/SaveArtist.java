package com.example.podcast;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.podcast.Model.Login;
import com.example.podcast.Model.LoginResponse;
import com.example.podcast.Model.Token;
import com.example.podcast.prefs.SessionPrefs;
import com.example.podcast.prefs.TokenPrefs;
import com.example.podcast.retrofit.ArtistService;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SaveArtist extends AppCompatActivity {

    ArtistService artistService;

    private EditText mName;
    private EditText mDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_artist);

        mName = (EditText) findViewById(R.id.artist_name);
        mDescription = (EditText)findViewById(R.id.artist_description);

        Button mbtnSave = (Button) findViewById(R.id.artist_btn_save);
        mbtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isOnline()) {
                    showLoginError(getString(R.string.error_network));
                    return;
                }
                attemptLogin();
            }
        });
    }

    private void showLoginError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }


    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        final String email = mEmailView.getText().toString();
        final String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
//            mAuthTask = new UserLoginTask(email, password);
//            mAuthTask.execute((Void) null);
            final Login login = new Login(email,password);

            Call<LoginResponse> call = userService.login(login);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    showProgress(false);
                    if(!response.isSuccessful()) {
                        String error = null;
                        try {
                            Gson gson = new Gson();
                            LoginResponse errorResponse = gson.fromJson(response.errorBody().string(), LoginResponse.class);
                            error = errorResponse.getMessage();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        showLoginError(error);
                        return;
                    }
                    LoginResponse loginResponse = response.body();
                    //Guardar login en SharedPrefer
                    SessionPrefs.getInstance(LoginActivity.this).saveLogin(loginResponse.getUser());

                    //Log.e("RESPUESTA Body", ""+loginResponse.getUser());

                    Call<Token> token = userService.token(email, password,true);
                    token.enqueue(new Callback<Token>() {
                        @Override
                        public void onResponse(Call<Token> call, Response<Token> response) {
                            //Log.e("Token", ""+response.body());
                            if(response.isSuccessful()){
                                Token token1 = response.body();
                                TokenPrefs.getInstance(LoginActivity.this).saveToken(token1);

                                showAppointmentsScreen();
                            }
                        }

                        @Override
                        public void onFailure(Call<Token> call, Throwable t) {
                            Log.i("RESPUESTA Failed", ""+t);
                            showProgress(false);
                            showLoginError(t.getMessage());
                        }
                    });


                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.i("RESPUESTA Failed", ""+t);
                    showProgress(false);
                    showLoginError(t.getMessage());
                }
            });
        }
    }

}
