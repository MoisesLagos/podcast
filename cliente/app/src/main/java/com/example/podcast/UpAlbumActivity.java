package com.example.podcast;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.style.UpdateLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.podcast.Adapter.AlbumAdapter;
import com.example.podcast.Adapter.AlbumCollection;
import com.example.podcast.Adapter.AristCollection;
import com.example.podcast.Adapter.ArtistsAdapter;
import com.example.podcast.Model.Album;
import com.example.podcast.prefs.SessionPrefs;
import com.example.podcast.prefs.TokenPrefs;

import java.util.ArrayList;
import java.util.List;

public class UpAlbumActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private List<Album> albumList = new ArrayList<>();
    private GridView gridView;
    private AlbumAdapter albumAdapter;

    private ArtistsAdapter artistsAdapter;
    private TextView userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_up_album);

        Toolbar toolbar2 = (Toolbar) findViewById(R.id.toolbar2);
        toolbar2.setTitle("Tus Artistas");
        setSupportActionBar(toolbar2);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_artist);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar2, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_artist);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        userName = (TextView) headerView.findViewById(R.id.nav_user_name);
        userName.setText(SessionPrefs.getInstance(this).getUser().getEmail());


//        gridView = (GridView) findViewById(R.id.grid_album);
//        albumAdapter = new AlbumAdapter(AlbumCollection.getAlbum());
//        gridView.setAdapter(albumAdapter);

        artistsAdapter = new ArtistsAdapter(AristCollection.getArtist());

        RecyclerView recyclerView = findViewById(R.id.recycler_artist);
        int numberColumns = 2;
        recyclerView.setLayoutManager(new GridLayoutManager(this, numberColumns));
        recyclerView.setAdapter(artistsAdapter);
        recyclerView.setNestedScrollingEnabled(false);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_artist);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

        } else {
            super.onBackPressed();


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Log.e("Nav", "button active");
            SessionPrefs.getInstance(UpAlbumActivity.this).clear();
            TokenPrefs.getInstance(UpAlbumActivity.this).clear();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_edit) {
            Intent i = new Intent(this,RegisterActivity.class);
            startActivity(i);
            finish();
            // Handle the camera action
        } else if (id == R.id.nav_upload) {

        } else if (id == R.id.nav_podcast) {
            Intent i = new Intent(this,MainActivity.class);
            startActivity(i);
            finish();

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_artist);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void artist_btn_crear_artista(View view){
        Intent i = new Intent(this,SaveArtist.class);
        startActivity(i);
        finish();

    }
}
