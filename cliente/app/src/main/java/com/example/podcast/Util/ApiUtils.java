package com.example.podcast.Util;

import com.example.podcast.retrofit.RetrofitClient;
import com.example.podcast.retrofit.UserService;

import retrofit2.Retrofit;

public class ApiUtils {
    private ApiUtils(){}
    private static final String BASE_URL = "http://192.168.1.110:3977/api/";

    public static UserService getUserService(){
        return RetrofitClient.getClient(BASE_URL).create(UserService.class);
    }
}
