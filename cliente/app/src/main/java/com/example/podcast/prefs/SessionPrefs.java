package com.example.podcast.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import com.example.podcast.Model.User;

public class SessionPrefs{

    private static final String SHARED_PREF_NAME = "login_preff";

    private static SessionPrefs mPrefs;
    private Context mCtx;

    public static final String PREF_USER_ID = "PREF_USER_ID";
    public static final String PREF_USER_NAME = "PREF_USER_NAME";
    public static final String PREF_USER_SURNAME = "PREF_USER_SURNAME";
    public static final String PREF_USER_EMAIL = "PREF_USER_EMAIL";
    public static final String PREF_USER_PASSWORD = "PREF_USER_PASSWORD";
    public static final String PREF_USER_ROLE = "PREF_USER_ROLE";
    public static final String PREF_USER_IMAGE = "PREF_USER_IMAGE";
    public static final String PREF_USER_PHONE = "PREF_USER_PHONE";
    public static final String PREF_USER_TOKEN = "PREF_USER_TOKEN";

    public static final String PREF_USER_LOGGED = "PREF_USER_LOGGED";

    private boolean mIsLoggedIn = false;

    private SessionPrefs(Context mCtx){
        this.mCtx = mCtx;
    }


    public static synchronized SessionPrefs getInstance(Context mCtx){
        if(mPrefs == null){
            mPrefs = new SessionPrefs(mCtx);
        }
        return mPrefs;
    }

    public void saveLogin(User user){

        if(user != null){
            SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            editor.putString(PREF_USER_ID, user.get_id());
            editor.putString(PREF_USER_NAME, user.getName());
            editor.putString(PREF_USER_SURNAME, user.getSurname());
            editor.putString(PREF_USER_EMAIL, user.getEmail());
            editor.putString(PREF_USER_ROLE, user.getRole());
            editor.putString(PREF_USER_IMAGE, user.getImage());
            editor.putString(PREF_USER_PASSWORD, user.getPassword());
            editor.putString(PREF_USER_PHONE, user.getPhone());

            editor.putBoolean(PREF_USER_LOGGED, true);
            //mIsLoggedIn = true;

            editor.apply();
        }
    }

    public boolean isLoggedIn(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(PREF_USER_LOGGED, false);
        //return mIsLoggedIn;
    }

    public User getUser() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new User(
                sharedPreferences.getString(PREF_USER_ID, null),
                sharedPreferences.getString(PREF_USER_NAME, null),
                sharedPreferences.getString(PREF_USER_SURNAME, null),
                sharedPreferences.getString(PREF_USER_EMAIL, null),
                sharedPreferences.getString(PREF_USER_ROLE,null),
                sharedPreferences.getString(PREF_USER_IMAGE, null),
                sharedPreferences.getString(PREF_USER_PASSWORD, null),
                sharedPreferences.getString(PREF_USER_PHONE, null)
        );
    }

    public void clear() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

}
