package com.example.podcast.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.podcast.LoginActivity;
import com.example.podcast.Model.Token;

public class TokenPrefs {
    private static final String SHARED_PREF_NAME = "token_preff";

    private static TokenPrefs mPrefs;
    private Context context;

    public static final String PREF_TOKEN = "PREF_TOKEN";

    private TokenPrefs(Context context){
        this.context = context;
    }

    public static synchronized TokenPrefs getInstance(Context context){
        if(mPrefs == null){
            mPrefs = new TokenPrefs(context);
        }
        return mPrefs;
    }

    public void saveToken(Token token){
        if(token !=null){
            SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            //Log.e("Token PREF", ""+token.getToken());
            editor.putString(PREF_TOKEN, token.getToken());
            editor.apply();
        }
    }

    public Token getToken(){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new Token(
                sharedPreferences.getString(PREF_TOKEN, null)
        );
    }

    public void clear(){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}
