package com.example.podcast.retrofit;

import com.example.podcast.Model.ArtistResponse;
import com.example.podcast.Model.Token;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ArtistService {

    @FormUrlEncoded
    @POST("artist")
    Call<ArtistResponse> saveArtist(@Field("name")String name, @Field("description") String description);
}
