package com.example.podcast.retrofit;

import android.util.Log;

import com.example.podcast.Model.Login;
import com.example.podcast.Model.LoginResponse;
import com.example.podcast.Model.Token;
import com.example.podcast.Model.User;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;

public interface UserService {

    @FormUrlEncoded
    @POST("register")
    Call<LoginResponse> registerUser(@Field("name") String name,
                            @Field("surname") String surname,
                            @Field("email") String email,
                            @Field("password") String password ,
                            @Field("role")String role,
                            @Field("phone") String phone);

    @POST("login")
    Call<LoginResponse> login(@Body Login loginBody);

    @FormUrlEncoded
    @POST("login")
    Call<Token> token(@Field("email")String email, @Field("password") String password, @Field("gethash") Boolean hash);
}
