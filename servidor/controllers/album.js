'use strict'
var fs = require('fs');
var path = require('path');

var Artist = require('../models/artist');
var Album = require('../models/album');
var Song = require('../models/song');

var mongoosePaginate = require('mongoose-pagination');

function getAlbum(req, res){
    var albumId = req.params.id;
    
    Album.findById(albumId).populate({path: 'artist'}).exec((err, album)=>{
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }else{
            if(!album){
                res.status(404).send({message: 'No existe el album'});
            }else{
                res.status(500).send({album});
            }
        }
    });
}
function getAlbums(req, res){
    var artistId = req.params.artist;

    if(!artistId){
        //sacar todos los album de la base de datos
        var find = Album.find({}).sort('title');
    }else{
        //sacar los album de un artista en concreto de la base de datos
        var find = Album.find({artist: artistId}).sort('year');
    }
    find.populate({path: 'artist'}).exec((err, albums)=>{
        if(err){
            res.status(500).send({message:'Error en la petición'});
        }else{
            if(!albums){
                res.status(404).send({message:'No hay albums en la base de datos'});
            }else{
                res.status(200).send({albums});
            }
        }
    });
}
function saveAlbum(req, res){
    var album = new Album();

    var params = req.body;

    album.title = params.title;
    album.description = params.description;
    album.year = params.year;
    album.image = 'null';
    album.artist = params.artist;

    album.save((err, albumStored)=>{
        if(err){
            res.status(500).send({message:'Error en la petición'})
        }else{
            if(!albumStored){
                res.status(404).send({message: 'El album no ha sido guardado'});
            }else{
                res.status(200).send({ album: albumStored });
            }
        }
    });
}
function updateAlbum(req,res){
    var albumId = req.params.id;
    var updateAlbum = req.body;

    Album.findByIdAndUpdate(albumId, updateAlbum, (err, albumUpdated)=>{
        if(err){
            res.status(500).send({message:'Error en la petición'})
        }else{
            if(!albumUpdated){
                res.status(404).send({message: 'El album no se ha actualizado'});
            }else{
                res.status(200).send({album:albumUpdated});
            }
        }
    });
}
function deleteAlbum(req, res){
    var albumId = req.params.id;

    Album.findByIdAndRemove(albumId, (err, albumRemoved)=>{
        if(err){
            res.status(500).send({message:'Error al eliminar el album'});
        }else{
            if(!albumRemoved){
                res.status(404).send({message:'El artista no se ha podido eliminar'});
            }else{
                
                Song.find({album:albumRemoved._id}).remove((err, songRemove)=>{
                    if(err){
                        res.status(500).send({message:'Error al eliminar la canción'});
                    }else{
                        if(!songRemove){
                            res.status(404).send({message:'El canción no se ha podido eliminar'});
                        }else{
                            res.status(200).send({album:albumRemoved});
                        }

                    }
                });
            }
        }
    });
}
function uploadImage(req, res){
    var albumId = req.params.id;
    var file_name = "subido";

    if(req.files){
        var file_path = req.files.image.path;
        var file_split = file_path.split('\\');
        var file_name = file_split[2];

        var ext_split = file_name.split('\.');
        var file_ext = ext_split[1];
        
        
        if(file_ext == 'png' || file_ext == 'jpg' || file_ext=='gif'){
            Album.findByIdAndUpdate(albumId, { image:file_name}, (err, albumUpdated)=>{
                if(err){
                    res.status(500).send({message : 'Error al actualizar la imagen del usuario'});
                }else{
                    if(!albumUpdated){
                        res.status(404).send({message : 'No se ha podido actualizar el usuario'});
                    }else{
                        res.status(200).send({album : albumUpdated});
                    }
                }
            });
        }else{
            res.status(200).send({message : 'Extensión del archivo no es correcto'});
        }


    }else{
        res.status(200).send({message : 'No ha subido ninguna imagen'});
    }
}
function getImageFile(req, res){
    var imageFile = req.params.imageFile;
    var path_file = './uploads/albums/'+imageFile;

    fs.exists(path_file, function(exists){
        if(exists){
            res.sendFile(path.resolve(path_file));
        }else{  
            res.status(200).send({message:'No existe la imagen'});
        }
    });
}

module.exports = {
    getAlbum,
    saveAlbum,
    getAlbums,
    updateAlbum,
    deleteAlbum,
    uploadImage,
    getImageFile
}